* bitlbee plugin for APRS

This plugin adds support for Bob Bruninga's amateur radio protocol
APRS to [[https://www.bitlbee.org][bitlbee]].  It connects to [[http://aprs-is.net/][APRS internet servers]] or other
software behaving like one.

Since messages you send to these servers might be transmitted on
regulated radio frequencies to reach their target, an amateur radio
license and an associated callsign from your country's radio
regulation authority is required to actually pass packets to these
servers.  To satisfy these regulations, the servers require your
callsign and a matching passcode to send packets.

,----[ http://aprs-is.net/Connecting.aspx ]
| It is the responsibility of each software author to provide the proper
| passcode to their individual users on a request basis.
`----

That's your's truly - so if you want to use this plugin with APRS
servers and haven't gotten a passcode from someone else yet, I can
generate one for you provided that you can convince me that you own a
radio callsign.

** Building

Dependencies for building this plugin are the same as bitlbee itself.
Having bitlbee and its build-dependencies installed on your system,
the following should be enough to make the plugin available for
connections in bitlbee:

: make
: sudo make install

** Quickstart

First restart your bitlbee and ensure the plugin was found properly

: <andreas> plugin list
: <root> Plugin                          Version
: <root> aprs                            unreleased

Create a bitlbee account with this plugin

: <andreas> account add aprs CALLSIGN PASSCODE
: <root> Account successfully added with tag aprs
: <andreas> account aprs on
: <root> aprs - Logging in: Connecting...
: <root> aprs - Logging in: # aprsc 2.1.5-g8af3cdc
: <root> aprs - Logging in: Logged in
: <andreas> save
: <root> Configuration saved

And finally after

: <andreas> add aprs DM6AS
: <root> Adding `DM6AS' to contact list

... you can /msg or /query DM6AS, perhaps even via radio waves of an
IGate that has last heard this station.

** Features

- [X] Message passing with specific stations
- Connect to APRS using
   - [X] APRS-IS
   - [ ] AX25 network interfaces
   - [ ] direwolf
   - [ ] Xastir
   - [ ] TNCs
- [ ] Process and present available positioning info
- [ ] Support non-directed messages such as bulletins, alerts, reports
- [ ] Support station queries
- [ ] Support sending beacons

** Known Bugs

- I expect some use-after-free crashes when you reconnect while there
  are lots pending events
- I never ran valgrind on it

** Authors

Andreas Seltenreich <seltenreich@gmx.de>
