bitlbee-plugin-aprs.so : bitlbee-plugin-aprs.c
	gcc -Wall -O0 -g -o $@ -shared $< $$(pkg-config --cflags bitlbee)

install: bitlbee-plugin-aprs.so
	install -d $$(pkg-config --variable=plugindir  bitlbee)
	install $< $$(pkg-config --variable=plugindir  bitlbee)
	systemctl restart bitlbee

clean:
	rm -f bitlbee-plugin-aprs.so
