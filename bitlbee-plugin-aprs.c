#include <nogaim.h>
#include <events.h>
#include <stdlib.h>
#include <gmodule.h>
#include <stdbool.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <stdio.h>
#include <glib.h>
#include <assert.h>
#include <time.h>

#define APRS "APZBEE"
#define APRS_PLUGIN_NAME "bitlbee-plugin-aprs"
#define APRS_PLUGIN_VERSION "unreleased"
#define MAX_MESSAGE_LENGTH 67

#ifndef BITLBEE_ABI_VERSION_CODE
#error "The bitlbee you're building against is too old"
#endif

struct aprsconn {
     int socket;
     FILE *stream;
     struct groupchat *gc_messages;
     struct groupchat *gc_server;
};

static struct aprs_regular_expressions {
     GRegex *message;
     GRegex *replyack;
     GRegex *callsign;
     GRegex *classicack;
} re;

struct aprs_message {
     gchar *source;
     gchar *dest;
     gchar *addressee;
     gchar *msg;
     gchar *id;
     int xmit;
     int acks;
     int dupes;
     time_t t;
};

struct aprs_buddy_data {
     gchar *replyack_in;
     int replyack_out;
     GList *outbox;
     GList *inbox;
     int rej_count;
     int ack_count;
};

static void aprs_message_free(struct aprs_message *m)
{
     g_free(m->source);
     g_free(m->dest);
     g_free(m->addressee);
     g_free(m->msg);
     g_free(m->id);
     g_free(m);
}

static void aprs_message_log(struct im_connection *ic, struct aprs_message *m, const char *prefix)
{
     imcb_log(ic, "%s(%d)%s>%s::%-9s:%s%s",
	      prefix, m->xmit, m->source, m->dest, m->addressee, m->msg, m->id);
}

struct plugin_info *init_plugin_info(void)
{
	static struct plugin_info info = {
		BITLBEE_ABI_VERSION_CODE,
		"aprs",
		"unreleased",
		"Bitlbee plugin for APRS",
		NULL,
		NULL
	};
	re.message = g_regex_new(
	  "^(?:.*?})?([A-Z0-9-]+)>(AP[^:]+)::([A-Z0-9-]+) *:([^{]+)({.+)?$",
	  0, 0, NULL);

	re.replyack = g_regex_new(
	  "^{?(..?)}(..?)?$",
	  0, 0, NULL);

	re.callsign = g_regex_new(
	  "^((?>[1-9][A-Z][A-Z]?+[0-9]|[A-Z][2-9A-Z]?[0-9])[A-Z]{1,4}+)(-[0-9][0-9]?)?$",
	  0, 0, NULL);

	re.classicack = g_regex_new(
	  "^(ack|rej)(.{1,5})$",
	  0, 0, NULL);

	return &info;
}

/* static char *aprs_set_eval_call(set_t *set, char *value) */
/* { */
/* 	if (!g_regex_match(re.callsign, value, 0, 0)) */
/* 	     return SET_INVALID; */
/* 	return value; */
/* } */

static void aprs_init(account_t *acc)
{
/*      set_add(&acc->set, "user", acc->user, aprs_set_eval_call, acc); */
     set_add(&acc->set, "host", "rotate.aprs2.net", NULL, acc);
     set_add(&acc->set, "port", "14580", set_eval_int, acc);
     set_add(&acc->set, "filter", "t/m", NULL, acc);
     set_add(&acc->set, "path", "TCPIP*", NULL, acc);
     set_add(&acc->set, "tx", "on", NULL, acc);
     set_add(&acc->set, "retries", "10", set_eval_int, acc);
}

static void aprs_logout(struct im_connection *ic)
{
     struct aprsconn *aprsconn = ic->proto_data;
     imcb_log(ic, "going down...");

     if (NULL == aprsconn)
	  return;

     proxy_disconnect(aprsconn->socket);

     g_free(aprsconn);
     ic->proto_data = NULL;
}


static void aprs_tx(struct im_connection *ic, struct aprs_message *m)
{
     struct aprsconn *ac = ic->proto_data;

     if (!set_getbool(&ic->acc->set, "tx")) {
	  imcb_log(ic, "not sending a message due to tx being off");
	  return;
     }

     if (m->xmit > set_getint(&ic->acc->set, "retries")) {
	  aprs_message_log(ic, m, "retry limit exceeded: ");
	  return;
     }

     aprs_message_log(ic, m, "sending: ");

     fprintf(ac->stream,
	     "%s>%s,%s::%-9s:%s%s\n",
	     ic->acc->user,
	     APRS,
	     set_getstr(&ic->acc->set, "path"),
	     m->addressee,
	     m->msg,
	     m->id ? m->id : "");

     m->xmit++;
}


/* process received acks */
/* return TRUE (don't pass to user) if classic ack, FALSE for reject or replyack */
static gboolean aprs_process_ack(struct im_connection *ic, struct aprs_buddy_data *bd, struct aprs_message *m)
{
     GMatchInfo *mi;

     if (m->id) {
	  if (g_regex_match(re.replyack, m->id, 0, &mi)) {
	       if (bd->replyack_in)
		    g_free(bd->replyack_in);
	       bd->replyack_in = g_match_info_fetch(mi, 1);

	       gchar *id = g_match_info_fetch(mi, 2);
	       if (!id) {
		    /* replyack-capable client but not acknowledgment
		     * present */
		    return FALSE;
	       }
	       assert(strlen(id) == 1
		      || strlen(id) == 2 /* regexp */);

	       for (GList *l = bd->outbox; l; l = l->next) {
		    struct aprs_message *sent = l->data;
		    if (!sent->id)
			 continue;
		    assert(strlen(sent->id) >= 4); /* we only put these in the list */

		    if (sent->id[1] == id[0]
			 && sent->id[2] == id[1])
		    {
			 aprs_message_log(ic, sent, "delivered: ");
			 aprs_message_free(sent);
			 bd->outbox = g_list_delete_link(bd->outbox, l);
			 break;
		    }
	       }

	       g_free(id);
	  }
	  g_match_info_free(mi);
	  return FALSE; /* replyack always has a payload */
     }

     if (g_regex_match(re.classicack, m->msg, 0, &mi)) {
	  gchar *acktype = g_match_info_fetch(mi, 1);
	  gchar *id = g_match_info_fetch(mi, 2);
	  g_match_info_free(mi);

	  /* prune the outbox */
	  for (GList *l = bd->outbox; l; l = l->next) {
	       struct aprs_message *sent = l->data;
	       if (!sent->id)
		    continue;

	       if (!strcmp(1+sent->id, id))
	       {
		    aprs_message_log(ic, sent, "delivered: ");
		    aprs_message_free(sent);
		    bd->outbox = g_list_delete_link(bd->outbox, l);
		    break;
	       }
	  }
	  g_free(id);

	  if (!strcmp(acktype, "rej")) {
	       /* not consuming rejects will let the user know the
		* station is not message capable */
	       g_free(acktype);
	       bd->rej_count++;
	       return FALSE;
	  }
	  g_free(acktype);
	  bd->ack_count++;
	  return TRUE;
     } else
	  g_match_info_free(mi);

     return FALSE;

}

/* send a classic ACK for message */
static void aprs_ack(struct im_connection *ic, struct aprs_message *m)
{
     struct aprsconn *ac = ic->proto_data;

     assert(m->id && *(m->id));

     if (!set_getbool(&ic->acc->set, "tx")) {
	  imcb_log(ic, "suppressing an ACK due to tx being off");
	  return;
     }

     aprs_message_log(ic, m, "sending ACK for: ");

     fprintf(ac->stream,
	     "%s>%s,%s::%-9s:ack%s\n",
	     ic->acc->user,
	     APRS,
	     set_getstr(&ic->acc->set, "path"),
	     m->source,
	     1+m->id);
     m->acks++;
}

static int aprs_message_compare(struct aprs_message *a, struct aprs_message *b)
{
     return
	    strcmp(a->source	, b->source)
	  || strcmp(a->addressee, b->addressee)
	  || strcmp(a->msg	, b->msg)
	  || (!a->id) ^ (!b->id)
	  || (a->id && b->id && strcmp(a->id, b->id));

}

static void aprs_parse_message(struct im_connection *ic, char *line)
{
     struct aprsconn *aprsconn = ic->proto_data;
     assert(aprsconn);

     struct aprs_message *m = g_new0(struct aprs_message, 1);

     {
	  GMatchInfo *mi;
	  if (!g_regex_match(re.message, line, 0, &mi)) {
	       g_match_info_free(mi);
	       return;
	  }

	  m->source    = g_match_info_fetch(mi, 1);
	  m->dest      = g_match_info_fetch(mi, 2);
	  m->addressee = g_match_info_fetch(mi, 3);
	  m->msg       = g_match_info_fetch(mi, 4);
	  m->id        = g_match_info_fetch(mi, 5);

	  g_match_info_free(mi);
     }

     if (!bee_user_by_handle(ic->bee, ic, m->source)) {
	  imcb_add_buddy(ic, m->source, NULL);
	  imcb_rename_buddy(ic, m->source, m->dest);
     }

     struct bee_user *bu = bee_user_by_handle(ic->bee, ic, m->source);
     assert(bu);
     struct aprs_buddy_data *bd = bu->data;
     assert(bd);

     if (aprsconn->gc_messages) {
	  imcb_chat_add_buddy(aprsconn->gc_messages, m->source);
	  sprintf(line, "%s: %s", m->addressee, m->msg);
	  imcb_chat_msg(aprsconn->gc_messages, m->source, line, 0, 0);
     }

     if (0 == strcmp(ic->acc->user, m->addressee)) {
	  m->t = time(NULL);

	  GList *dupe =
	       g_list_find_custom(
		    bd->inbox,
		    m,
		    (GCompareFunc) aprs_message_compare);

	  if (dupe) {
	       ((struct aprs_message *)(dupe->data))->dupes++;
	       imcb_log(ic, "dupe: <%s> %s", m->source, m->msg);
	  } else {
	       if (!aprs_process_ack(ic, bd, m))
		    imcb_buddy_msg(ic, m->source, m->msg, 0, 0);
	  }

	  if (m->id) {
	       aprs_ack(ic, m); /* send a APRS 1.0 ack */
	  }

	  if (dupe)
	       aprs_message_free(m);
	  else
	       bd->inbox = g_list_append(bd->inbox, m);
     } else {
	  aprs_message_free(m);
     }
}

static gboolean aprs_event(gpointer data, gint fd, b_input_condition cond)
{
     struct im_connection *ic = data;
     char *line = NULL;
     size_t len = 0;
     ssize_t nread;
     const char *hostname = set_getstr(&ic->acc->set, "host");

     if (!ic) return TRUE;

     struct aprsconn *aprsconn = ic->proto_data;

     if (!aprsconn) return TRUE;

     if (!aprsconn->stream) return TRUE;

     if ((nread = getline(&line, &len, aprsconn->stream)) > 0) {
	  if ('#' == line[0]) {
	       if (aprsconn->gc_server)
		    imcb_chat_msg(aprsconn->gc_server, hostname, line, 0, 0);
	  } else {
	       aprs_parse_message(ic, line);
	  }
     }

     g_free(line);

     if (feof(aprsconn->stream))
	  imc_logout(ic, FALSE);

     return TRUE; /* remove event */
}

static gboolean aprs_connected(gpointer data, gint source, b_input_condition cond)
{
     struct im_connection *ic = data;
     struct aprsconn *aprsconn = ic->proto_data;

     if (source == -1) {
	  imcb_error(ic, "Could not connect to server: %s", strerror(errno));
	  imc_logout(ic, TRUE);
	  return FALSE;
     }

     aprsconn->stream = fdopen(aprsconn->socket, "rw+");

     if (NULL == aprsconn->stream) {
	  imcb_error(ic, "fdopen failed: %s", strerror(errno));
	  imc_logout(ic, FALSE);
	  return TRUE;
     }

     /* expecting a welcome message */
     {
	  char *line = NULL;
	  size_t len = 0;

	  if ((getline(&line, &len, aprsconn->stream)) > 0) {
	       imcb_log(ic, "%s", line);
	  }
	  g_free(line);
     }

     fprintf(aprsconn->stream, "user %s pass %s vers %s %s filter %s\n",
	     ic->acc->user,
	     ic->acc->pass,
	     APRS_PLUGIN_NAME,
	     APRS_PLUGIN_VERSION,
	     set_getstr(&ic->acc->set, "filter"));

     imcb_connected(ic);

     b_input_add(aprsconn->socket, B_EV_IO_READ, aprs_event, ic);

     return TRUE;
}

static void aprs_login(account_t *acc)
{
     imcb_new(acc);
     imcb_log(acc->ic, "Connecting...");

     struct aprsconn *aprsconn = g_new0(struct aprsconn, 1);
     acc->ic->proto_data = aprsconn;

     aprsconn->socket =
	  proxy_connect(set_getstr(&acc->set, "host"),
			set_getint(&acc->set, "port"),
			aprs_connected,
			acc->ic);

     if (aprsconn->socket == -1) {
	  imcb_error(acc->ic, "Could not connect to server: %s", strerror(errno));
	  imc_logout(acc->ic, TRUE);
	  return;
     }

}

static void aprs_process_outbox(struct im_connection *ic, GList *outbox)
{
     if (!outbox)
	  return;

     struct aprs_message *m = outbox->data;
     assert(m->t);

     if (!m->xmit) {
	  aprs_tx(ic, m);
	  return;
     }

     /* retransmit? */
     time_t now = time(NULL);
     assert(((time_t) -1) != now);
     assert(((time_t) -1) != m->t);

     if (((1+m->xmit) * (1+m->xmit) * 10) <= (now - m->t)) {
	  aprs_tx(ic,m);
	  return;
     }

     /* schedule timer event for potential retransmit */
}

static int aprs_buddy_msg(struct im_connection *ic, char *to, char *message, int flags)
{
     struct aprs_message *m = g_new0(struct aprs_message, 1);
     struct bee_user *bu = bee_user_by_handle(ic->bee, ic, to);
     struct aprsconn *ac = ic->proto_data;
     assert(ac);

     if (strlen(message) > MAX_MESSAGE_LENGTH) {
	  imcb_error(ic, "Message too long (%ld > %d), discarding.",
		     strlen(message),
		     MAX_MESSAGE_LENGTH);
	  return 0;
     }

     if (!bu) {
	  imcb_add_buddy(ic, m->source, NULL);
	  bu = bee_user_by_handle(ic->bee, ic, to);
     }

     assert(bu);
     assert(bu->data);

     struct aprs_buddy_data *bd = bu->data;

     m->source = g_strdup(ic->acc->user);
     m->dest = g_strdup(set_getstr(&ic->acc->set, "path"));
     m->addressee = g_strdup(to);
     m->msg       = g_strdup(message);

     m->id        = g_new0(char, 10);
     g_snprintf(m->id, 10, "{%02x}%s", bd->replyack_out++,
		bd->replyack_in ? bd->replyack_in : "");
     m->t = time(NULL);

     bd->outbox = g_list_append(bd->outbox, m);

     imcb_log(ic, "Queueing message %s", m->id);
     aprs_process_outbox(ic, bd->outbox);

     return 0;
}

static void aprs_set_away(struct im_connection *ic, char *state, char *message)
{
     imcb_log(ic, "Not implemented: aprs_set_away");
}

static void aprs_add_buddy(struct im_connection *ic, char *name, char *group)
{
     imcb_add_buddy(ic, name, group);
}

static void aprs_remove_buddy(struct im_connection *ic, char *name, char *group)
{
     imcb_remove_buddy(ic, name, group);
}

static void aprs_keepalive(struct im_connection *ic)
{
	GSList *l;

	for (l = ic->bee->users; l; l = l->next) {
	     bee_user_t *bu = l->data;
	     assert(bu->ic == ic);
	     struct aprs_buddy_data *bd = bu->data;
	     if (!bd) continue;
	     if (!bd->outbox) continue;
	     imcb_log(ic, "%d message(s) pending for %s",
		      g_list_length(bd->outbox),
		      bu->handle);
	     aprs_process_outbox(ic, bd->outbox);
	}

	return;
}

static void aprs_chat_list_item_free(gpointer data)
{
     bee_chat_info_t *chat_info = data;
     g_free(chat_info);
}

/* This sets/updates the im_connection->chatlist field with a
 * bee_chat_info_t GSList. This function should ensure the
 * bee_chat_list_finish() function gets called at some point
 * after the chat list is completely updated.
 */
static void aprs_chat_list(struct im_connection *ic, const char *server)
{

     if(ic->chatlist)
	  g_slist_free_full(ic->chatlist, aprs_chat_list_item_free);
     ic->chatlist = NULL;

     bee_chat_info_t *chat_info = g_new0(struct bee_chat_info, 1);
     chat_info->title = "messages";
     chat_info->topic = "all messages go here";
     ic->chatlist = g_slist_append(ic->chatlist, chat_info);

     chat_info = g_new0(struct bee_chat_info, 1);
     chat_info->title = "server";
     chat_info->topic = "non-PACKETs received from the server";
     ic->chatlist = g_slist_append(ic->chatlist, chat_info);

     imcb_chat_list_finish(ic);
}

/* This is used when the user uses the /join #channel IRC command.  If
 * your protocol does not support publicly named group chats, then do
 * not implement this. */
static struct groupchat *aprs_chat_join(struct im_connection *ic, const char *room,
	                    const char *nick, const char *password, set_t **sets)
{
     struct aprsconn *aprsconn = ic->proto_data;
     const char *hostname = set_getstr(&ic->acc->set, "host");
     if (!aprsconn)
	  return NULL;

     if (!strcmp(room, "server")) {
	  if (aprsconn->gc_server) {
	       imcb_chat_add_buddy(aprsconn->gc_server, ic->acc->user);
	       return aprsconn->gc_server;
	  }
	  aprsconn->gc_server = imcb_chat_new(ic, "server");
	  imcb_chat_name_hint(aprsconn->gc_server, "server");
	  imcb_chat_add_buddy(aprsconn->gc_server, hostname);
	  imcb_chat_add_buddy(aprsconn->gc_server, ic->acc->user);
	  return aprsconn->gc_server;
     }

     if (!strcmp(room, "messages")) {
	  if (aprsconn->gc_messages) {
	       imcb_chat_add_buddy(aprsconn->gc_messages, ic->acc->user);
	       return aprsconn->gc_messages;
	  }
	  aprsconn->gc_messages = imcb_chat_new(ic, "messages");
	  imcb_chat_name_hint(aprsconn->gc_messages, "messages");
	  imcb_chat_add_buddy(aprsconn->gc_messages, ic->acc->user);
	  return aprsconn->gc_messages;
     }

     return NULL;
}

/* This is called when the user sends a message to the groupchat.
 * 'flags' may be ignored. */
static void aprs_chat_msg(struct groupchat *c, char *message, int flags)
{
     imcb_error(c->ic, "Not implemented: %s()", "aprs_chat_msg");
     return;
}

static GList *aprs_away_states(struct im_connection *ic)
{
     GList *s = 0;
     s = g_list_append(s, "Off Duty");
     s = g_list_append(s, "En Route");
     s = g_list_append(s, "In Service");
     s = g_list_append(s, "Returning");
     s = g_list_append(s, "Committed");
     s = g_list_append(s, "Special");
     s = g_list_append(s, "Priority");
     return s;
}

static void aprs_chat_add_settings(account_t *acc, set_t **head)
{
     set_add(head, "path", "TCPIP*", NULL, NULL);
}

static void aprs_buddy_data_add(struct bee_user *bu)
{
     bu->data = g_new0(struct aprs_buddy_data, 1);
}

static void aprs_buddy_data_free(struct bee_user *bu)
{
     struct aprs_buddy_data *bd = bu->data;

     g_list_free_full(bd->inbox, (GDestroyNotify)aprs_message_free);
     g_list_free_full(bd->outbox, (GDestroyNotify)aprs_message_free);

     g_free(bd->replyack_in);
     bd->replyack_in = NULL;
     g_free(bu->data);
     bu->data = NULL;
}

void init_plugin(void)
{
	struct prpl *ret = g_new0(struct prpl, 1);

	ret->options = PRPL_OPT_NOOTR | OPT_SELFMESSAGE;
	ret->name = "aprs";
	ret->init = aprs_init;
	ret->login = aprs_login;
	ret->logout = aprs_logout;
	ret->buddy_msg = aprs_buddy_msg;
	ret->add_buddy = aprs_add_buddy;
	ret->remove_buddy = aprs_remove_buddy;
	ret->chat_list = aprs_chat_list;
	ret->chat_msg = aprs_chat_msg;
	ret->chat_join = aprs_chat_join;
	ret->chat_add_settings = aprs_chat_add_settings;
/* 	ret->chat_free_settings = aprs_chat_add_settings; */
	ret->set_away = aprs_set_away;
	ret->away_states = aprs_away_states;
/* 	ret->chat_leave = aprs_chat_leave; */
/* 	ret->get_info = aprs_get_info; */
/* /\* 	ret->add_permit = aprs_add_permit; *\/ */
/* /\* 	ret->add_deny = aprs_add_deny; *\/ */
/* /\* 	ret->rem_permit = aprs_rem_permit; *\/ */
/* /\* 	ret->rem_deny = aprs_rem_deny; *\/ */
	ret->buddy_data_add = aprs_buddy_data_add;
	ret->buddy_data_free = aprs_buddy_data_free;
	ret->handle_cmp = g_ascii_strcasecmp;
	ret->handle_is_self = NULL;
	ret->keepalive = aprs_keepalive;
	register_protocol(ret);
}
